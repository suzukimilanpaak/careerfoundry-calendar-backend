Hello,

I will first explain how to run two projects as I created them for the frontend and backend respectively. And, later I will write some notes about my architectural decisions.

# Product Review

Please click the image to play video.

[![Demo Video](https://bitbucket.org/suzukimilanpaak/careerfoundry-calendar-backend/raw/eac0491682db634cfcfce62de56043407b8d5b63/public/images/demo-video.png)](https://youtu.be/g4ROABS4U48)

# How to Run Projects

### Frontend(careerfoundry-calendar)

- To start project

```shell
npm install
npm start
# => It will automatically open http://localhost:3000/ in your browser
```

- No test written


### Backend(careerfoundry-calendar-backend)

- To start project

```shell
bundle install
bundle exec padrino start --port 4000

curl http://localhost:4000/slots
```

- To run spec

```
bundle exec rspec
rubocop
```

* it's in the middle


# Architecture

- Frontend(careerfoundry-calendar): create-react-app
- Backend(careerfoundry-calendar-backend): padrino

I would have a single project normally to make integration tests and e2e tests easy. However, since this is a programming challenge and I wanted something easy to start, I introduced [create-react-app](https://github.com/facebook/create-react-app) as a dedicated project for the frontend.

Accordingly, the responsibility of the backend project will be minimal, limited to respond to data in the database in JSON format. Therefore, I chose [padrino](http://padrinorb.com/), which is an extension of Sinatra and have flexible choices in engines for ORM, view, CSS etc... This time, I chose ActiveRecord only. Components for View and CSS were not plugged in to keep the project minimal.


# Things I Didn’t

Since this is a coding challenge for a limited time, I didn’t do things I would normally do.


### Integration tests(contract tests)
I would have contract tests in-between the frontend and backend projects so that anyone can confirm either doesn’t break the other.


### BDD & E2E tests
I would have Capybara tests on top of unit tests so that I can confirm things are working on the end-users side.


### Endpoint for Healthcheck
I could also have an endpoint in the backend project for a deep health check. It is used by regular heartbeat to check microservices are up and running, and also for checking if microservices are being alive while rolling deployment.


### Others
- Specs for a helper in the backend project
- Tests in the frontend project
- Creating Calendar component in the frontend project
- Requests from frontend to backend are uncompleted


That’s it. Thank you very much for taking time for this.
