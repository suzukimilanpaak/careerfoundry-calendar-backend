# frozen_string_literal: true

source 'https://rubygems.org'

# Project requirements
gem 'padrino', '0.15.0'
gem 'rake', '13.0.1'

# Component requirements
gem 'activerecord', '>= 3.1', require: 'active_record'
gem 'json', '2.3.1'
gem 'pagy', '3.8.3'
gem 'sqlite3'

group :test, :development do
  gem 'pry', '0.12.2'
  gem 'pry-nav', '0.3.0'
end

# Test requirements
group :test do
  gem 'database_cleaner-active_record', '1.8.0'
  gem 'dotenv', '2.7.6'
  gem 'factory_bot', '4.8.2'
  gem 'rack-test', require: 'rack/test'
  gem 'rspec', '3.9.0'
  gem 'rubocop-rspec', '1.42.0'
  gem 'simplecov', '0.18.5'
  gem 'webmock', '3.9.1'
end

git_source(:github) { |repo_name| "https://github.com/#{repo_name}" }
