# frozen_string_literal: true

require 'date'

FactoryBot.define do
  factory :slot do
    sequence(:starts_at, DateTime.now)
  end
end
