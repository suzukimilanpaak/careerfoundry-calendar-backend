# frozen_string_literal: true
require 'simplecov'
SimpleCov.start

RACK_ENV = 'test' unless defined?(RACK_ENV)
require File.expand_path(File.dirname(__FILE__) << '/../config/boot')
Dir[File.expand_path(File.dirname(__FILE__) << '/../app/helpers/**/*.rb')].sort.each(&method(:require))
require 'database_cleaner/active_record'

RSpec.configure do |config|
  config.include Rack::Test::Methods

  # FactoryBot
  config.include FactoryBot::Syntax::Methods
  config.before(:suite) do
    FactoryBot.find_definitions
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end
end

# DatbaseCleaner
DatabaseCleaner.strategy = :truncation
DatabaseCleaner.clean

# You can use this method to custom specify a Rack app
# you want rack-test to invoke:
#
#   app CareerfoundryCalendarBackend::App
#   app CareerfoundryCalendarBackend::App.tap { |a| }
#   app(CareerfoundryCalendarBackend::App) do
#     set :foo, :bar
#   end
#
def app(app = nil, &blk)
  @app ||= block_given? ? app.instance_eval(&blk) : app
  @app ||= Padrino.application
end
