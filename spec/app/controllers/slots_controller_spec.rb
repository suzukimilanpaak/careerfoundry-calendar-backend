# frozen_string_literal: true

require 'spec_helper'
require 'json'

describe '/slots' do
  describe '#index' do
    context 'when starts_at is not given' do
      before do
        create(:slot, starts_at: DateTime.new(2020, 11, 15))
        create(:slot, starts_at: DateTime.new(2020, 11, 16))
        get '/slots'
      end

      it { expect(JSON.parse(last_response.body).size).to eq(2) }
    end

    context 'when starts_at is given' do
      before do
        create(:slot, starts_at: DateTime.new(2020, 11, 15))
        create(:slot, starts_at: DateTime.new(2020, 11, 16))
        get '/slots?starts_at=2020-11-16'
      end

      it { expect(JSON.parse(last_response.body).size).to eq(1) }
    end

    context 'when starts_at is invalid' do
      before do
        get '/slots?starts_at=_invalid_'
      end

      it { expect(JSON.parse(last_response.body).first).to eq(400) }
    end
  end

  describe '#create' do
    context 'when starts_at is valid' do
      before do
        post '/slots', { starts_at: '2020-11-15' }
      end

      it { expect(last_response.body).to eq('') }
    end
  end
end
