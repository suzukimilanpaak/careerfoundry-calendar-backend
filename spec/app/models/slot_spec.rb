# frozen_string_literal: true

require 'spec_helper'

describe Slot do
  describe '#for_date' do
    subject(:actual) do
      described_class.for_date('2020-11-15').all
    end

    before do
      [
        DateTime.new(2020, 11, 15, 0, 0, 0, 'UTC'),
        DateTime.new(2020, 11, 16, 0, 0, 0, 'UTC')
      ].each { |dt| create(:slot, starts_at: dt) }
    end

    it { expect(actual.last.starts_at.day).to eq(15) }
  end
end
