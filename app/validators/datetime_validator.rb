# frozen_string_literal: true

require 'date'

# Validator for datetime attributes
class DatetimeValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return if value.is_a?(DateTime)

    error = begin
      DateTime.parse(value)
    rescue StandardError
      ArgumentError
    end
    if error == ArgumentError
      record.errors[attribute] << (options[:message] || 'must be a valid datetime')
    end
  end
end
