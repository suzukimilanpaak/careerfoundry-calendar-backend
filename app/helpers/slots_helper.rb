# frozen_string_literal: true

require 'json'

# Name space for the app
module CareerfoundryCalendarBackend
  # The application
  class App
    # Module for Slots
    module SlotsHelper
      def render400(exc)
        logger.error(exc) if exc
        [400, { 'code': 400, 'summary': 'Bad Request' }, ['']].to_json
      end
    end

    helpers SlotsHelper
  end
end
