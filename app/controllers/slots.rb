# frozen_string_literal: true

require 'json'

# Slots controller
CareerfoundryCalendarBackend::App.controllers :slots, providers: :json do
  get :index do
    if params[:starts_at].nil? || params[:starts_at].empty?
      Slot.all.to_json
    else
      Slot.for_date(params[:starts_at]).all.to_json
    end
  rescue ArgumentError => e
    return render400(e)
  end

  post '/' do
    slot = Slot.new(params[:starts_at])
    if slot.valid? && slot.save
      slot.to_json
    else
      render400
    end
  end
end
