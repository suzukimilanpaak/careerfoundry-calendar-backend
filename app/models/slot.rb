# frozen_string_literal: true

# Slot model
class Slot < ActiveRecord::Base
  scope :for_date, lambda { |datetime|
    datetime = datetime.is_a?(DateTime) ? datetime : Date.strptime(datetime).to_datetime
    where(starts_at: datetime)
  }

  validates :starts_at, datetime: true
end
