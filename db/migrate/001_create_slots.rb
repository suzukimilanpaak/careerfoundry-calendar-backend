# frozen_string_literal: true

# Migration for slots table
class CreateSlots < ActiveRecord::Migration[6.0]
  def self.up
    create_table :slots do |t|
      t.datetime :starts_at
      t.timestamps null: false
    end
  end

  def self.down
    drop_table :slots
  end
end
